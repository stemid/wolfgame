#!/usr/bin/env bash

set -euo pipefail
set -x

cmd_help() {
  echo "Usage: $0"
}

cmd_build() {
  buildah bud -f "tools/container/worker/Containerfile" -t "wolfgame:latest-celery" .
  buildah bud -f "tools/container/server/Containerfile" -t "wolfgame:latest" .
}

cmd_network() {
  podman network exists wolfgame-internal || podman network create --internal wolfgame-internal
  podman network exists wolfgame-external || podman network create wolfgame-external
}

cmd_volume() {
  podman volume exists wolfgame-data || podman volume create wolfgame-data
}

cmd_start() {
  cmd_network
  cmd_volume
  pod_name=wolfgame
  new=''

  if ! podman pod exists "$pod_name"; then
    new='new:'
  fi

  podman run -d --name=redis --pod "$new$pod_name" \
    --network wolfgame-internal --restart=always \
    docker.io/redis:6.2-alpine

  podman run -d --name=celery --pod "$pod_name" \
    --env-file=.env.production \
    --network wolfgame-internal --restart=always \
    localhost/wolfgame:latest-celery
  
  podman run -d --name=wolfgame-init --pod "$pod_name" \
    --env-file=.env.production -v wolfgame-data:/app \
    --network wolfgame-external \
    localhost/wolfgame:latest python3 -m wolfgame.register

  podman run -d --name=server --pod "$pod_name" \
    --env-file=.env.production -v wolfgame-data:/app \
    --restart=always --network wolfgame-internal \
    --network wolfgame-external \
    localhost/wolfgame:latest
}

cmd_up() {
  cmd_build
  cmd_start
}

cmd_stop() {
  podman pod stop wolfgame
}

cmd_down() {
  cmd_stop
}

cmd_destroy() {
  cmd_stop
  if podman pod exists wolfgame; then
    podman pod stop wolfgame
    podman pod rm wolfgame
  fi
  podman volume exists wolfgame-data && podman volume rm wolfgame-data
  podman network exists wolfgame-internal && podman network rm wolfgame-internal
  podman network exists wolfgame-external && podman network rm wolfgame-external
}

cmd_purge() {
  cmd_destroy
  buildah rmi wolfgame:latest
  buildah rmi celery:latest
}

command=$1

case "$command" in
  "" | "-h" | "--help")
    cmd_help
    ;;
  *)
    shift
    "cmd_${command}" $@
    if [ $? = 127 ]; then
      echo "Error: $command is unknown command" >&2
    fi
    ;;
esac
