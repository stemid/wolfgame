from pprint import pprint as pp
from mastodon import StreamListener


def make_cmd(notification):
    sender = notification.status.account.acct
    tags = notification.status.tags
    if not len(tags):
        raise ValueError('No tags provided')

    cmd = tags.pop(0).name
    tags = [t.name for t in tags]
    arguments = {
        'status': notification.status
    }

    return cmd, sender, arguments


class GamebotListener(StreamListener):

    def __init__(self, game, frontend):
        super(GamebotListener, self).__init__()
        self.game = game
        self.frontend = frontend
        self.l = self.game.l


    def on_notification(self, notification):
        if notification.status.visibility == 'direct':
            try:
                (cmd, sender, args) = make_cmd(notification)
            except ValueError:
                self.l.error('No tags provided')
                return False

            self.game.dispatch_command2(
                cmd,
                sender,
                **args
            )

