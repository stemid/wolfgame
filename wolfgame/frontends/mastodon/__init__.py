from pprint import pprint as pp
from jinja2 import Environment, PackageLoader, select_autoescape
from mastodon import Mastodon
from wolfgame import GameEngine, CommandBase
from .listeners import GamebotListener


class MastodonCommands(CommandBase):

    def __init__(self, **kw):
        super().__init__(**kw)
        self.mastodon = kw.get('mastodon')
        self.env = kw.get('template_env')


    def cmd_gameinfo(self, sender, **kw):
        gameinfo_tpl = self.env.get_template('gameinfo.html')
        gameinfo = gameinfo_tpl.render(
            game=self.game,
            player=sender,
            botinfo=self.mastodon.me()
        )

        #print(gameinfo)

        try:
            reply = self.mastodon.status_post(
                gameinfo,
                in_reply_to_id=kw.get('status'),
                visibility='direct'
            )
            self.l.info('Sent content:{content}'.format(
                content=reply.content
            ))
        except Exception as e:
            self.l.error('status_post failed: {error}'.format(
                error=str(e)
            ))


    def cmd_start(self, sender, **kw):
        # TODO: Create a template for when a new game is started.
        #       And one for if a game is already running, maybe
        #       just use gameinfo.html.
        raise NotImplemented


class MastodonFrontend(object):

    def __init__(self, app_name, **kw):
        self.l = kw.get('logger')
        self.base_url = kw.get('base_url')

        self.game = GameEngine('mastodon-wolfgame', **kw)
        self.mastodon = Mastodon(
            access_token='{app_name}-user_creds.secret'.format(
                app_name=app_name
            ),
            api_base_url=self.base_url
        )

        self._streams = []
        self.template_env = Environment(
            loader=PackageLoader('wolfgame.frontends.mastodon', 'templates'),
            autoescape=select_autoescape(['html'])
        )


    @property
    def streams(self):
        return self._streams


    async def init_frontend(self):
        """Init frontend and game engine"""
        command_template = MastodonCommands(
            engine=self.game,
            mastodon=self.mastodon,
            template_env=self.template_env
        )
        await self.game.init_game(command_template=command_template)
        self.setup_streams()


    async def periodic_callback(self):
        """Check liveness of streams"""
        for stream in self.streams:
            if not stream.is_alive():
                self.l.info('Stream is dead, running setup_streams() again')
                self.setup_streams()

        # Sync game data
        await self.game.sync_game_data()


    def setup_streams(self):
        if len(self.streams):
            self.l.info('Cleaning up old streams')
            for stream in self.streams:
                try:
                    stream.close()
                    self._streams.remove(stream)
                except ValueError:
                    continue
                except Exception as e:
                    raise

        # Add streams
        listener = GamebotListener(self.game, self)
        sh = self.mastodon.stream_user(
            listener,
            run_async=True,
            reconnect_async=True
        )
        self._streams.append(sh)


