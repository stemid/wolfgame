# https://github.com/prometheus/client_python/pull/308/files 😞

from ._exposition import MetricsHandler

__all__ = ['MetricsHandler']
