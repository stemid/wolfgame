import importlib
import pkgutil

import arrow
import wolfgame.commands
from wolfgame.storage import RedisBackend
from wolfgame.game import Game
from wolfgame.errors import *


APP_NAME = 'wolfgame'

def iter_namespace(ns):
    """Helper function to iterate over modules in a specific namespace"""
    return pkgutil.iter_modules(ns.__path__, '{name}.'.format(
        name=ns.__name__
    ))


class CommandBase(object):

    def __init__(self, **kw):
        self.engine = kw.get('engine')
        self.game = self.engine.game
        self.l = self.engine.l


    def cmd_gameinfo(self, sender, **kw):
        if self.game.active:
            self.l.info((
                'created:{time}, state:{state}, round:{round}'
            ).format(
                time=arrow.get(
                    self.game.created
                ).humanize(),
                state=self.game.state,
                round=self.game.round
            ))
            self.l.info('players:{players}'.format(
                players=', '.join(self.game.players)
            ))
        else:
            self.l.info('No game active')


    def cmd_start(self, sender, **kw):
        self.l.info('Start game command issued by {player}'.format(
            player=sender
        ))

        if self.game.active:
            self.l.warn('Game already active')
            return

        self.game.start_game(sender)


    def cmd_join(self, sender):
        try:
            self.game.join_player(sender)
        except GamePlayerAlreadyJoined as e:
            self.l.warn(str(e))
        except Exception as e:
            self.l.error(str(e))


class GameEngine(object):

    def __init__(self, app_name, **kw):
        self.app_name = app_name
        self._commands = {}
        self.l = kw.get('logger')
        self.redis_url = kw.get('redis_url')

        self.storage = RedisBackend(self.redis_url)
        self.game = Game()


    async def init_game(self, command_template: object = None):
        # Load command plugins
        for finder, name, ispkg in iter_namespace(wolfgame.commands):
            _cmd = importlib.import_module(name)
            _cmd_name = _cmd.command_name
            self._commands[_cmd_name] = {
                'module': _cmd,
                'name': name
            }

        self.l.info(self._commands)

        if command_template:
            self.command = command_template
        else:
            self.command = CommandBase(engine=self)

        await self.storage.init()
        await self.load_game_data()


    async def load_game_data(self):
        game_data = await self.storage.get_dict(self.app_name)

        if game_data:
            self.game.from_dict(game_data)


    async def sync_game_data(self):
        if self.game.expired:
            self.l.info('Ending expired game')
            self.game.end_game()

        game_data = self.game.to_dict()
        await self.storage.set_dict(self.app_name, game_data)


    def dispatch_command(self, command: str, sender: str, **kw: dict):
        """
        TODO: Replace with dispatch_command2
        """

        cmd = getattr(
            self.command,
            'cmd_{command}'.format(
                command=command
            ),
            None
        )

        if cmd:
            try:
                cmd(sender, **kw)
            except Exception as e:
                self.l.error('{command} failed: {error}'.format(
                    command=command,
                    error=str(e)
                ))


    def dispatch_command2(self, command: str, sender: str, **kw: dict):
        """ This is used by frontend when it receives a command.

        Parameters
        ----------
        command : str
            Simple string command used as key for an internal dict of commands
        sender : str
            String used to identify sender of command
        kw : dict
            Dict of additional arguments specific to the frontend
        """

        if command in self._commands:
            try:
                _inst = self._commands[command]['module'].Command(
                    engine=self,
                    sender=sender,
                    **kw
                )
            except Exception as e:
                raise CommandInitError(str(e))

            try:
                _inst.run()
            except Exception as e:
                raise CommandRunError(str(e))

