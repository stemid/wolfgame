from . import CommandBase

command_name = 'gameinfo'

class Command(CommandBase):

    def __init__(self, engine, sender, **kw):
        self.game = engine
        self.l = self.game.l
        self.sender = sender
        self.args = kw

    def run(self):
        self.l.info(self.sender, self.args)
