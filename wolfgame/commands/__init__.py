
class CommandBase(object):
    command_name = 'base'

    def __init__(self, **kw):
        self.engine = kw.get('engine')
        self.game = self.engine.game
        self.l = self.engine.l


    def __str__(self):
        return command_name


    def run(self):
        raise NotImplemented
