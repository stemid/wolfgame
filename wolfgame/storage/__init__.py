import aioredis
import json


class RedisBackend(object):

    def __init__(self, redis_url, **kw):
        self.encoding = kw.get('encoding', 'utf-8')
        self.redis_url = redis_url


    async def init(self):
        self.r = aioredis.from_url(
            self.redis_url
        )


    async def get_list(self, name, default_value=None):
        data = await self.r.get(name)

        if not data and isinstance(default_value, list):
            return default_value

        if data:
            return json.loads(data)
        return data


    async def get_dict(self, name, default_value=None):
        data = await self.r.get(name)

        if not data and isinstance(default_value, dict):
            return default_value

        if data:
            return json.loads(data)
        return data


    async def set_dict(self, name, value):
        await self.r.set(name, json.dumps(value))

