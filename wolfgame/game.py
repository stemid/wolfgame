from datetime import datetime

from .errors import *


"""
game = {
    "created": str<iso8601_datetime>,
    "state": str<starting|started|ended>,
    "players": [
        {
            "account": {
                "id": 12345,
                "username": "stemid",
                "role": <villager|werewolf|...>
            }
        },
    ],
    "round": int<0-10>,
}
"""

MINIMUM_PLAYERS = 3

class Game(object):

    def __init__(self):
        self._data = {
            'created': datetime.utcnow().isoformat(),
            'state': 'starting',
            'players': [],
            'round': 0
        }


    @property
    def created(self):
        return datetime.fromisoformat(self._data.get('created'))

    @property
    def state(self):
        return self._data.get('state')

    @property
    def players(self):
        return self._data.get('players')

    @players.setter
    def players(self, val: list):
        self._data['players'] = val

    @property
    def round(self):
        return self._data.get('round')

    @property
    def active(self):
        if self._data.get('state') != 'ended':
            return True
        return False

    @active.setter
    def active(self, val: str):
        if val in ['starting', 'started', 'ended']:
            self._data['state'] = val

    @property
    def missing_players(self):
        if len(self.players) < MINIMUM_PLAYERS:
            return int(MINIMUM_PLAYERS - len(self.players))
        return 0

    @property
    def expired(self):
        if not self.active:
            return False

        if self.state == 'starting':
            td = datetime.utcnow() - self.created
            if td.seconds > 3600:
                return True


    def from_dict(self, game_data: dict):
        self._data.update(game_data)

    def to_dict(self):
        return self._data


    def start_game(self, player):
        self._data.update({
            'state': 'starting',
            'created': datetime.utcnow().isoformat(),
            'players': [player]
        })


    def end_game(self):
        self._data.update({
            'state': 'ended',
            'players': [],
            'round': 0
        })

    def join_player(self, player):
        if player not in self.players:
            self.players = self.players + player
        else:
            raise GamePlayerAlreadyJoined(
                '{player} already inplayers list'.format(
                    player=player
                )
            )

