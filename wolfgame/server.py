import functools
from sys import exit

import click
import tornado.autoreload
import tornado.web
from tornado.ioloop import IOLoop
#from prometheus_client import Counter

from wolfgame import APP_NAME
from wolfgame.log import ApplicationLog
from wolfgame.frontends.mastodon import MastodonFrontend

from wolfgame.prometheus import MetricsHandler


@click.command()
@click.option(
    '--url',
    envvar='MASTODON_URL',
    help='Mastodon base URL'
)
@click.option(
    '--redis-url',
    default='redis://localhost:6379/0',
    envvar='REDIS_URL',
    help='Redis url, defaults to redis://localhost:6379/0'
)
@click.option(
    '--debug',
    default=False,
    is_flag=True,
    envvar='DEBUG',
    help='Run in development mode with autoreload'
)
@click.option(
    '--prometheus',
    default=False,
    is_flag=True,
    envvar='PROMETHEUS',
    help='Serve prometheus metrics'
)
@click.option(
    '--prometheus-port',
    default=int(9001),
    envvar='PROMETHEUS_PORT',
    help='Default prometheus metrics port: 9001'
)
def run_server(url, redis_url, debug, prometheus, prometheus_port):
    """
    Start the server.

    TODO: Re-write to handle other frontends like Twitter or IRC.

    Parameters
    ----------
    url : str
        Mastodon instance base URL
    redis_url : str
        Redis connection URL
    debug : bool
        Enable autoreload and debug output
    """

    if not url:
        ctx = click.get_current_context()
        click.echo(ctx.get_help())
        ctx.exit()

    l = ApplicationLog(APP_NAME, debug=debug).logger
    mf = MastodonFrontend(
        APP_NAME,
        base_url=url,
        logger=l,
        redis_url=redis_url
    )

    # Set to use asyncio
    IOLoop.configure('tornado.platform.asyncio.AsyncIOLoop')

    io_loop = IOLoop.current()

    # Run at startup
    init_frontend_cb = functools.partial(mf.init_frontend)
    io_loop.add_callback(init_frontend_cb)

    # Run periodically every X ms
    periodic_cb = tornado.ioloop.PeriodicCallback(
        callback=mf.periodic_callback,
        callback_time=5000
    )
    periodic_cb.start()

    if debug:
        tornado.autoreload.add_reload_hook(
            lambda: l.info('Code changed, reloading server')
        )
        tornado.autoreload.add_reload_hook(mf.setup_streams)
        tornado.autoreload.start()

    # Init prometheus
    if prometheus:
        prometheus_app = tornado.web.Application([
            (r'/metrics', MetricsHandler)
        ])
        prometheus_app.listen(prometheus_port)
        #test_counter = Counter('my_failures', 'Description of counter')
        #test_counter.inc()

    # Start main infinite loop
    try:
        if debug:
            l.info('Starting server with {redis_url}'.format(
                redis_url=redis_url
            ))
            if prometheus:
                l.info('Prometheus on http://localhost:{port}'.format(
                    port=prometheus_port
                ))
        io_loop.start()
    except KeyboardInterrupt as e:
        pass


if __name__ == '__main__':
    exit(run_server(None))

