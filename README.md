# Mastodon Wolfgame

Work in progress wolfgame for Mastodon.

# Local dev setup

## Virtualenv

    python -m venv .venv
    source .venv/bin/activate

## Dependencies

    pip install -r requirements.txt

## Run background job runner

    docker-compose -f wolfgame-compose.yml up -d redis celery

## Populate environment

The arguments for the cli commands usually all take an environment variable instead.

* ``REDIS_URL`` - Example: redis://127.0.0.1:6379/1
* ``CELERY_BROKER_URL`` - Example: redis://127.0.0.1:6379/1
* ``MASTODON_URL`` - Example: https://fediverse.tld
* ``MASTODON_NAME`` - Name of robot account
* ``MASTODON_LOGIN`` - Login username of robot
* ``MASTODON_PASSWORD`` - Login password of robot

## Register on a Mastodon instance

1. First create an account for your game robot.
2. Store username and password in environment.
3. Then use the ``wolfgame.register`` command to create API tokens.

    python -m wolfgame.register

## Run in venv

Depends on the tokens generated in the previous step.

    python -m wolfgame.server --debug

# Run in Docker

Store your environment variables in ``.env.production`` and run this.

    docker-compose -f wolfgame-compose.yml up

# TODO

- [ ] More game content: commands, actions, template files, user roles, testing.
- [ ] Prometheus metrics.
