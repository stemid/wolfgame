# Wolfgame high level design

This serves as a reminder to myself since I return to this project after almost a year. The whole design is focused on supporting multiple frontends. A frontend being a place to play the game, like IRC, Twitter, Lemmy or Mastodon.

So this design attempts to be frontend agnostic but as of writing only Mastodon is implemented as a sort of reference implementation.

> One fantasy for the long term would be for one game server instance to play the same game over multiple frontends. The drawback is that players can't communicate and scheme among themselves across frontends, like twitter -> irc, or mastodon -> twitter, but commands are only ever sent to the bot so it wouldn't be impossible to just play along.

## Step 1: Connect

See the README for how to register a bot account and connect with it. Starting the server will also start a listener for Mastodon.

> The use of Mastodon is hard coded into ``wolfgame.server.py`` until I have time to work on other frontends. The idea is that this could be a configuration setting so one server instance can support one frontend.

> Each frontend can have a periodic_callback set in the main Tornado setup in ``wolfgame.server.py`` that either performs standard cleaning, anything the specific frontend needs, or nothing at all.

> At init the frontend creates an instance of the ``wolfgame.GameEngine`` class.

## Step 2: Message the bot

Interact with the bot using tags as commands, for example message it ``@bot #gameinfo`` and it should respond with current gameinfo.

> Each frontend should have its own method of handling incoming messages but for now we only do Mastodon.

### Mastodon

In ``wolfgame.frontends.mastodon.listener.py`` you have a stream setup listening for events from Mastodon using the Python Mastodon library. It then signals using ``on_notification`` to ``wolfgame.GameEngine.dispatch_command2()``.

## Step 3: Dispatch command

The game engine, or the core of the bot, then has a loaded list of command plugins from ``wolfgame.commands``.

The sent command ``#gameinfo`` matches ``wolfgame.commands.gameinfo.py`` and runs it, this simply adds one or more new actions in the list ``actions`` of its own instance.

This action is how the frontend should respond to the command, if at all. Game engine commands can do other things too of course like keep stats or perform admin actions like reload the game server.

In this case it just adds a new action called ``message``, this name matches an action defined in the frontend.

## Step 4: Frontend responds (Under construction)

Question: Who runs the next action?

* I don't think it should go back to ``wolfgame.frontends.mastodon.listener.py``, its work is done.
* Suggestion: periodic callback in main thread that runs all queued actions.
* ``wolfgame.GameEngine.dispatch_command2()`` is non-blocking so it should only add the next action to a queue.
* TODO: Investigate if ``wolfgame.GameEngine.dispatch_command2()`` can add the next action to a queue that will instantly trigger a blocking async task from ``wolfgame.server.py``.
* OR if we should simply use a periodic callback every second that checks a queue of actions. By default now, before any Task runner, it could just call the ``mastodon.actions.message.run()`` function of each action. But later a Task runner could potentially run this function, given the right environment.

``wolfgame.frontends.mastodon.actions.message.py`` responds in much the same way as the gameinfo command, but all frontend actions are specific to their frontend. So an IRC frontend would send an IRC message with a response, the mastodon frontend sends a mastodon reply.

In this case it takes an argument which is the sender name, the mastodon user it replies to.

### Why have frontend actions defined standalone?

The goal there is to eventually be able to run them as commands so that they can be off loaded to a job runner like rq or celery, for scaling. I'm not implementing scaling off the bat, just keeping the option open by separating them into standalone action classes that should be able to run standalone.
